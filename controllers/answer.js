const Answer = require("../models/Answer");
const Question = require("../models/Question");
const CustomError = require("../helpers/error/CustomError");
const asyncErrorWrapper = require("express-async-handler");

const addNewAnswerToQuestion = asyncErrorWrapper(async (req, res, next) => {
  const question_id = req.params.id;
  const user_id = req.user.id;
  const { content } = req.body;

  const answer = await Answer.create({
    content,
    question: question_id,
    user: user_id,
  });

  return res.status(200).json({
    success: true,
    data: answer,
  });
});

const getAllAnswersByQuestion = asyncErrorWrapper(async (req, res, next) => {
  const question_id = req.params.id;

  const question = await Question.findById(question_id).populate("answers");

  return res.status(200).json({
    success: true,
    count: question.answers.length,
    data: question.answers,
  });
});

const getSingleAnswer = asyncErrorWrapper(async (req, res, next) => {
  const answer = req.data;

  return res.status(200).json({
    success: true,
    data: answer,
  });
});

const editAnswer = asyncErrorWrapper(async (req, res, next) => {
  const answer = req.data;
  const { content } = req.body;

  answer.content = content;
  await answer.save();

  return res.status(200).json({
    success: true,
    data: answer,
  });
});

const deleteAnswer = asyncErrorWrapper(async (req, res, next) => {
  const answer = req.data;

  await answer.remove();

  return res.status(200).json({
    success: true,
    message: "Answer deleted successfully.",
  });
});

const likeAnswer = asyncErrorWrapper(async (req, res, next) => {
  const answer = req.data;
  const userId = req.user.id;
  const isUserLiked = answer.likes.includes(userId);

  if (!isUserLiked) {
    answer.likes.push(userId);
  } else {
    answer.likes.splice(answer.likes.indexOf(userId), 1);
  }

  await answer.save();

  return res.status(200).json({
    success: true,
    message: isUserLiked
      ? "You unliked this answer successfully."
      : "You liked this answer successfully.",
    data: answer,
  });
});

module.exports = {
  addNewAnswerToQuestion,
  getAllAnswersByQuestion,
  getSingleAnswer,
  editAnswer,
  deleteAnswer,
  likeAnswer,
};
