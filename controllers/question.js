const Question = require("../models/Question");
const CustomError = require("../helpers/error/CustomError");
const asyncErrorWrapper = require("express-async-handler");

const getAllQuestions = asyncErrorWrapper(async (req, res, next) => {
  const questions = await Question.find();

  res.status(200).json({
    success: true,
    data: questions,
  });
});

const askNewQuestion = asyncErrorWrapper(async (req, res, next) => {
  const information = req.body;

  const question = await Question.create({
    ...information,
    user: req.user.id,
  });

  res.status(200).json({
    success: true,
    data: question,
  });
});

const getSingleQuestion = asyncErrorWrapper(async (req, res, next) => {
  const question = req.data;

  res.status(200).json({
    success: true,
    data: question,
  });
});

const editQuestion = asyncErrorWrapper(async (req, res, next) => {
  const questionId = req.params.id;
  const { title, content } = req.body;

  const question = await Question.findByIdAndUpdate(
    questionId,
    { title, content },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json({
    success: true,
    data: question,
  });
});

const deleteQuestion = asyncErrorWrapper(async (req, res, next) => {
  const questionId = req.params.id;

  const question = await Question.findByIdAndDelete(questionId);

  res.status(200).json({
    success: true,
    message: "Question deleted successfully.",
  });
});

const likeQuestion = asyncErrorWrapper(async (req, res, next) => {
  const question = req.data;
  const userId = req.user.id;
  const isUserLiked = question.likes.includes(userId);

  if (!isUserLiked) {
    question.likes.push(userId);
  } else {
    question.likes.splice(question.likes.indexOf(userId), 1);
  }

  await question.save();

  res.status(200).json({
    success: true,
    message: isUserLiked
      ? "You unliked this question successfully."
      : "You liked this question successfully.",
    data: question,
  });
});

module.exports = {
  getAllQuestions,
  askNewQuestion,
  getSingleQuestion,
  editQuestion,
  deleteQuestion,
  likeQuestion,
};
