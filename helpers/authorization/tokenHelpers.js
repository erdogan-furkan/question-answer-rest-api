const sendJwtToClient = (user, res) => {
  const { JWT_EXPIRE, NODE_ENV } = process.env;

  const token = user.generateJwtFromUser();

  return res
    .status(200)
    .cookie("access_token", token, {
      httpOnly: true,
      secure: NODE_ENV === "development" ? false : true,
      expires: new Date(Date.now() + parseInt(JWT_EXPIRE)),
    })
    .json({
      success: true,
      access_token: token,
      data: {
        name: user.name,
        email: user.email,
      },
    });
};
const isTokenIncluded = (req) => {
  return (
    req.headers.authorization && req.headers.authorization.startsWith("Bearer:")
  );
};
const getAccessTokenFromHeader = (req) => {
  return req.headers.authorization.split(" ")[1];
};

module.exports = {
  sendJwtToClient,
  isTokenIncluded,
  getAccessTokenFromHeader,
};
