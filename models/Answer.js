const Question = require("./Question");
const mongoose = require("mongoose");
const asyncErrorWrapper = require("express-async-handler");

const AnswerSchema = new mongoose.Schema({
  content: {
    type: String,
    required: [true, "Please provide a content."],
    minlength: [10, "Please provide a content at least 10 character."],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  likes: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "User",
    },
  ],
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true,
  },
  question: {
    type: mongoose.Schema.ObjectId,
    ref: "Question",
    required: true,
  },
});

AnswerSchema.pre("save", async function (next) {
  if (!this.isModified("user")) return next();

  try {
    const question = await Question.findById(this.question);

    question.answers.push(this._id);

    await question.save();
    next();
  } catch (err) {
    return next(err);
  }
});

AnswerSchema.post("remove", async function (next) {
  try {
    const question = await Question.findById(this.question);

    question.answers.splice(question.answers.indexOf(this._id), 1);

    await question.save();
  } catch (err) {
    return next(err);
  }
});

module.exports = mongoose.model("Answer", AnswerSchema);
