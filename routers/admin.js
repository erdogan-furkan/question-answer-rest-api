const { Router } = require("express");
const { blockUser, deleteUser } = require("../controllers/admin");
const {
  checkUserExist,
} = require("../middlewares/database/databaseErrorHelpers");

const adminRouter = Router();

adminRouter.get("/block/:id", checkUserExist, blockUser);
adminRouter.delete("/user/:id", checkUserExist, deleteUser);

module.exports = adminRouter;
