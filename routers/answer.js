const { Router } = require("express");
const {
  addNewAnswerToQuestion,
  getAllAnswersByQuestion,
  getSingleAnswer,
  editAnswer,
  deleteAnswer,
  likeAnswer,
} = require("../controllers/answer");
const {
  checkQuestionAndAnswerExist,
} = require("../middlewares/database/databaseErrorHelpers");
const {
  getAccessToRoute,
  getAnswerOwnerAccess,
} = require("../middlewares/authorization/auth");

const answerRouter = Router({ mergeParams: true });

answerRouter.get("/", getAllAnswersByQuestion);
answerRouter.get("/:answer_id", checkQuestionAndAnswerExist, getSingleAnswer);
answerRouter.get(
  "/:answer_id/like",
  [getAccessToRoute, checkQuestionAndAnswerExist],
  likeAnswer
);
answerRouter.put(
  "/:answer_id",
  [getAccessToRoute, checkQuestionAndAnswerExist, getAnswerOwnerAccess],
  editAnswer
);
answerRouter.delete(
  "/:answer_id",
  [getAccessToRoute, checkQuestionAndAnswerExist, getAnswerOwnerAccess],
  deleteAnswer
);
answerRouter.post("/", getAccessToRoute, addNewAnswerToQuestion);

module.exports = answerRouter;
