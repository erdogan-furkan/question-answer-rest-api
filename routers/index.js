const { Router } = require("express");
const {
  getAccessToRoute,
  getAdminAccess,
} = require("../middlewares/authorization/auth");
const authRouter = require("./auth");
const questionsRouter = require("./question");
const userRouter = require("./user");
const adminRouter = require("./admin");

const rootRouter = Router();

rootRouter.use("/auth", authRouter);
rootRouter.use("/questions", questionsRouter);
rootRouter.use("/users", userRouter);
rootRouter.use("/admin", [getAccessToRoute, getAdminAccess], adminRouter);

module.exports = rootRouter;
