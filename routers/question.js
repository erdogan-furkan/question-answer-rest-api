const { Router } = require("express");
const answerRouter = require("./answer");
const {
  getAllQuestions,
  askNewQuestion,
  getSingleQuestion,
  editQuestion,
  deleteQuestion,
  likeQuestion,
} = require("../controllers/question");
const {
  getAccessToRoute,
  getQuestionOwnerAccess,
} = require("../middlewares/authorization/auth");
const {
  checkQuestionExist,
} = require("../middlewares/database/databaseErrorHelpers");

const questionRouter = Router();

questionRouter.get("/", getAllQuestions);
questionRouter.get("/:id", checkQuestionExist, getSingleQuestion);
questionRouter.get(
  "/:id/like",
  [getAccessToRoute, checkQuestionExist],
  likeQuestion
);
questionRouter.post("/ask", getAccessToRoute, askNewQuestion);
questionRouter.put(
  "/:id",
  [getAccessToRoute, checkQuestionExist, getQuestionOwnerAccess],
  editQuestion
);
questionRouter.delete(
  "/:id",
  [getAccessToRoute, checkQuestionExist, getQuestionOwnerAccess],
  deleteQuestion
);
questionRouter.use("/:id/answers", checkQuestionExist, answerRouter);

module.exports = questionRouter;
