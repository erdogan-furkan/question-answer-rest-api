const { Router } = require("express");
const { getSingleUser, getAllUsers } = require("../controllers/user");
const {
  checkUserExist,
} = require("../middlewares/database/databaseErrorHelpers");

const userRouter = Router();

userRouter.get("/", getAllUsers);
userRouter.get("/:id", checkUserExist, getSingleUser);

module.exports = userRouter;
