const express = require("express");
const dotenv = require("dotenv");
const path = require("path");
const connectDatabase = require("./helpers/database/connectDatabase");
const customErrorHandler = require("./middlewares/errors/customErrorHandler");

const rootRouter = require("./routers/index");

// Environment variables
dotenv.config({
  path: "./config/env/config.env",
});

// Database connection
connectDatabase();

const app = express();

// Express body middleware
app.use(express.json());

// Routes
app.use("/api", rootRouter);

// Static files
app.use(express.static(path.join(__dirname, "public")));

// Error handler
app.use(customErrorHandler);

app.listen(process.env.PORT, () => {
  console.log(`App started on PORT:${process.env.PORT}`);
});
